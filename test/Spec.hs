module Main where

import LinGrad


data Two a = Two a a
  deriving (Show,Functor,Foldable,Traversable)

main :: IO ()
main = do
  let r = descend Problem {
              basePoint = Two (1.0 , 2.0) (1.0 , 25.0)
            , constraints = \(Two x y) -> do
                assert $ x >== K 5
                assert $ x <== K 15

                assert $ y >== K 10
                assert $ y <== K 17
                
            , objective = \(Two x y) ->
                abs (y - x)
            , maxSteps = 100
            , resolution = (0.1 , 1.0 , 100000.0)
            }

  case r of
    Just (_ , Two x y , _) ->
      if x == y
         then pure ()
         else error "nonsense"
    _ -> error "nonsense"
  
