module LinGrad
  ( Problem (..)
  , type StepSize
  , descend
  , module Wengert
  , module LP
  ) where

import Control.Monad.ST
import Data.Coerce
import Data.Foldable
import Data.Vector.Packed.Mutable
import qualified Data.Vector.Generic.Mutable as MGV
import Foreign.C.Types
import GHC.Exts
import GHC.Generics hiding (S)
import GHC.Int
import LP
import LP.Problem (LProb)
import qualified LP.Problem
import LP.CBindings (CVar)
import LP.Encoding (encodeGLPK)
import qualified LP.CBindings as C
import LP.Monad
import Unsafe.Coerce
import Wengert


type StepSize = Double

data Problem t = Problem {
    basePoint :: t (StepSize , Double)
  , objective :: forall s . t (Var s Double) -> Var s Double
  , constraints :: forall s m . LCMonad s m => t (L s Double) -> m ()
  , maxSteps :: Int
  , resolution :: (Double , Double , Double) -- min / start / max, in steps
  }

{-# INLINE descend #-}
-- | Objective function value, destination point, gradient
descend :: Traversable t => Problem t -> Maybe (Double , t Double , t Double)
descend p = do
  let (Comp1 rs , lp) = getProblem do
            vs <- traverse (\m -> (,) m <$> newVar) (basePoint p)
            constraints p (snd <$> vs)
            pure $ Comp1 vs
  case resolution p of
    (min_res , s_res , max_res) -> if max_res >= 0
      then descend' rs lp (objective p) (maxSteps p) (max 0 min_res , max 0 s_res , max_res)
      else Nothing

{-# INLINABLE descend' #-}
descend' :: Traversable t
         => t ((StepSize , Double) , L CVar Double)
         -> LProb CVar
         -> (forall s . t (Var s Double) -> Var s Double)
         -> Int
         -> (Double , Double , Double)
         -> Maybe (Double , t Double , t Double)
descend' vs lp objf maxS res = do
  let !(!objv , !found , !r , g) = iterateg
           do objf
           do descend'' vs lp maxS res
           do snd . fst <$> vs
  if found then Just (objv,r,g) else Nothing


data S s = S {
    glpk :: {-# UNPACK #-} !(C.Unmanaged s)
  , cur :: {-# UNPACK #-} !(MVector s (Double , Int)) -- singeton, res / step, also used as local scratch
  , steps :: {-# UNPACK #-} !(MVector s StepSize)
  , checkpoint :: {-# UNPACK #-} !(MVector s Double)
  }

{-# INLINABLE descend'' #-}
-- unsolvables are tracked via cur, setting step count to max and step size to -1
descend'' :: Traversable t
   => t ((StepSize , Double) , L CVar Double) -> LProb CVar -> Int -> (Double , Double , Double)
   -> GState Bool S s Double
descend'' vs lp maxS (mi_res , s_res , ma_res) = GState
  do \vv -> do
      g <- C.newUnmanagedGLPK

      -- cur ref
      r_cur <- MGV.unsafeNew 2 -- so that the hack below has enough space even on 32-bit systems
      MGV.unsafeWrite r_cur 0 (s_res , 0)

      let !l = fromIntegral (LP.Problem.basicVarCount lp)

      stepv <- MGV.unsafeNew l

      values <- MGV.unsafeNew @_ @MVector l

      Comp1 cvs <- encodeGLPK (coerce <$> Comp1 vs) lp g
      for_ cvs \((step,x),cv) -> do
        (s,i) <- MGV.unsafeRead r_cur 0
        C.set_col_bounds g cv C.BD_BOTH (x - s_res * step) (x + s_res * step)
        MGV.unsafeWrite r_cur 0 (s,i+1)
        MGV.unsafeWrite stepv i step
        MGV.unsafeWrite values i x

      let s = S g r_cur stepv values

      findStart values s ma_res maxS False s_res

      for 0 (<) (MGV.length vv) \i ->
        C.get_col_prim g (cvar i) >>= MGV.unsafeWrite vv i

      pure s
  do \s !vv !pv -> do
      let !cr = cur s
      let !g = glpk s
      MGV.unsafeRead cr 0 >>= \case
        (cur_res,cur_step) -> if cur_step >= maxS
          then do
            C.freeUnmanaged g
            let !b = cur_res >= 0
            -- normalise the solution before exiting
            for 0 (<) (MGV.length vv) \i -> do
              v <- MGV.unsafeRead vv i
              MGV.unsafeWrite vv i if v == (-0.0) then 0.0 else v
            pure do Just b
          else do
            let !stepv = steps s

            for 0 (<) (MGV.length vv) \i -> do
              v <- MGV.unsafeRead vv i
              step <- MGV.unsafeRead stepv i
              p <- MGV.unsafeRead pv i
              C.set_col_bounds g (cvar i) C.BD_BOTH (v - cur_res * step) (v + cur_res * step)
              C.set_obj_coef g (cvar i) p

            csolve g False >>= \case
              1 -> do
                for 0 (<) (MGV.length vv) \i ->
                  C.get_col_prim g (cvar i) >>= MGV.unsafeWrite vv i
                if cur_step `mod` 4 == 3
                   then do
                     -- check if progress is happening at a reasonable rate (defined as having moved
                     -- at least 2 steps on any dimension in the last 4 iterations), and if not,
                     -- drop the resolution
                     let scratch :: MVector s Int
                         !scratch = unsafeCoerce cr

                     MGV.unsafeWrite scratch 0 0
                     MGV.unsafeWrite scratch 1 0
                     let !checkv = checkpoint s
                     for 0 (<) (MGV.length vv) \i -> do
                       step <- MGV.unsafeRead stepv i
                       v <- MGV.unsafeRead vv i
                       v_o <- MGV.unsafeRead checkv i
                       let !(D# threshold) = 2.0 * cur_res * step
                       let !(D# r) = abs (v_o - v)
                       MGV.unsafeWrite scratch (I# (threshold <## r)) 1
                       MGV.unsafeWrite checkv i v

                     MGV.unsafeRead scratch 1 >>= \case
                       0 -> do
                         MGV.unsafeWrite cr 0 (cur_res / 2.0 , 1 + cur_step)
                         if cur_res <= mi_res
                            then do
                              C.freeUnmanaged g
                              pure (Just True)
                            else pure Nothing
                       _ -> do
                         MGV.unsafeWrite cr 0 (cur_res , 1 + cur_step)
                         pure Nothing

                   else do
                     MGV.unsafeWrite cr 0 (cur_res , 1 + cur_step)
                     pure Nothing
              _ -> do
                -- this should never happen logically, but can if the problem has pathological
                -- structure that manifests floating point instability issues. in which case,
                -- just stop descent and give them the existing solution. probably should
                -- warn about this in some way, at least in debug mode.
                error "wrong" -- pure (Just True)

findStart :: MVector s Double -> S s -> Double -> Int -> Bool -> Double -> ST s ()
findStart values s ma_res max_s = go
  where
    go pre s_res = do
      let !stepv = steps s
      let !cr = cur s
      let !g = glpk s
      csolve g pre >>= \case
        1 -> MGV.unsafeWrite cr 0 (s_res , 0)
        _ -> do
          let new_res = min ma_res $ scaleK * s_res
          for 0 (<) (MGV.length values) \i -> do
            v <- MGV.unsafeRead values i
            step <- MGV.unsafeRead stepv i
            C.set_col_bounds g (cvar i) C.BD_BOTH (v - new_res * step) (v + new_res * step)
          if new_res < ma_res
             then do
               MGV.unsafeWrite cr 0 (new_res,0)
               go False new_res
             else do
               csolve g False >>= \case
                 1 -> MGV.unsafeWrite cr 0 (new_res,0)
                 _ -> MGV.unsafeWrite cr 0 (-1,max_s)

-- 1 = found feasible ; 0 = anything else
csolve :: C.Unmanaged s -> Bool -> ST s CInt
csolve g pre = C.solve g pre 2147483647 >>= \case
  C.S_ERR_OK -> C.get_primal_status g >>= \case
    C.SOL_FEASIBLE -> pure 1
    _ -> pure 0
  _ -> pure 0

{-# INLINE scaleK #-}
scaleK :: Double
scaleK = 2.0

{-# INLINE cvar #-}
cvar :: Int -> CVar
cvar x = case x + 1 of
  I# i -> C.CVar (CInt (I32# (intToInt32# i)))

{-# INLINE for #-}
-- non-allocating iterator
for :: Applicative f => Int -> (Int -> Int -> Bool) -> Int -> (Int -> f ()) -> f ()
for s b e f = loop s
  where
    loop i = if b i e
      then f i *> loop (i + 1)
      else pure ()

